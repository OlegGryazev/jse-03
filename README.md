#### Project Manager

###### Software requirements:
* JDK 8
* Apache Maven 3.6.3
    
###### Technology stack:
* Maven
* Java SE
    
###### Developer:
    Gryazev Oleg
    email: gryazev77@gmail.com
    
###### Build:
    mvn clean install
 
###### Run:
    java -jar target/jse-2.0.jar