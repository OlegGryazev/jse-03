package ru.gryazev.tm.repository;

import ru.gryazev.tm.entity.Task;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class TaskRepository {

    private List<Task> tasks = new ArrayList<>();

    public List<Task> getTaskByProjectId(String id){
        return tasks.stream().filter(o -> o.getProjectId().equals(id)).collect(Collectors.toList());
    }

    public Task findOne(String id){
        return tasks.stream().filter(o -> o.getId().equals(id)).findFirst().orElse(null);
    }

    public void merge(Task task){
        Task taskToMerge = findOne(task.getId());
        if (taskToMerge == null)
            add(task);
        else tasks.set(tasks.indexOf(taskToMerge), task);
    }

    public void add(Task task){
        tasks.add(task);
    }

    public void remove(String id){
        tasks.removeIf(o -> o.getId().equals(id));
    }

    public void removeTaskByProjectId(String id){
        tasks.removeIf(o -> o.getProjectId().equals(id));
    }

    public List<Task> getTasks() {
        return tasks;
    }

}
