package ru.gryazev.tm.repository;

import ru.gryazev.tm.entity.Project;

import java.util.ArrayList;
import java.util.List;

public class ProjectRepository {

    private List<Project> projects = new ArrayList<>();

    public void merge(Project project){
        projects.stream().filter(o -> o.getId().equals(project.getId())).peek(o -> o = project);
    }

    public List<Project> getProjects() {
        return projects;
    }

    public void add(Project project){
        if (project != null) projects.add(project);
    }

    public boolean remove(String id){
        return projects.removeIf(o -> o.getId().equals(id));
    }

}
