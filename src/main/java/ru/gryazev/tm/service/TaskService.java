package ru.gryazev.tm.service;

import ru.gryazev.tm.entity.Project;
import ru.gryazev.tm.entity.Task;
import ru.gryazev.tm.repository.ProjectRepository;
import ru.gryazev.tm.repository.TaskRepository;
import ru.gryazev.tm.util.ArrayUtils;
import ru.gryazev.tm.util.InputUtils;
import java.io.BufferedReader;
import java.io.IOException;
import java.util.Date;
import java.util.List;

public class TaskService {

    private TaskRepository taskRepository;

    private ProjectRepository projectRepository;

    public TaskService(ProjectRepository projectRepository, TaskRepository taskRepository) {
        this.taskRepository = taskRepository;
        this.projectRepository = projectRepository;
    }

    public void create(BufferedReader in) throws IOException {
        List<Project> projects = projectRepository.getProjects();

        System.out.println("[TASK CREATE]");

        int index = InputUtils.inputNumber(in, "project id");
        if (!ArrayUtils.indexExists(projects, index)) {
            System.out.println("Project not found");
            return;
        }
        Task task = InputUtils.getTaskFromConsole(in);
        task.setProjectId(projects.get(index).getId());
        taskRepository.add(task);
        System.out.println("[OK]");
    }

    public void view(BufferedReader in) throws IOException {
        Task task = taskRepository.findOne(getTaskId(in));
        if (task == null)
            return;

        System.out.println(task);
    }

    public void list(BufferedReader in) throws IOException{
        List<Project> projects = projectRepository.getProjects();

        int index = InputUtils.inputNumber(in, "project index");
        if (!ArrayUtils.indexExists(projects, index)){
            System.out.println("Project not found");
            return;
        }

        List<Task> tasks = taskRepository.getTaskByProjectId(projects.get(index).getId());
        for (int i = 0; i < tasks.size(); i++)
            System.out.println(i + ". " + tasks.get(i).getName());
    }

    public void edit(BufferedReader in) throws IOException {
        String taskId = getTaskId(in);
        if (taskId == null)
            return;

        Task editedTask = InputUtils.getTaskFromConsole(in);
        editedTask.setId(taskId);
        editedTask.setProjectId(taskRepository.findOne(taskId).getProjectId());

        taskRepository.merge(editedTask);

        System.out.println("[OK]");
    }

    public void remove(BufferedReader in) throws IOException {
        String taskId = getTaskId(in);
        if (taskId == null)
            return;

        taskRepository.remove(taskId);

        System.out.println("[DELETED]");
    }

    private String getTaskId(BufferedReader in){
        List<Project> projects = projectRepository.getProjects();
        int projectIndex = InputUtils.inputNumber(in, "project index");
        if (!ArrayUtils.indexExists(projects, projectIndex)){
            System.out.println("Project not found");
            return null;
        }

        List<Task> tasks = taskRepository.getTaskByProjectId(projects.get(projectIndex).getId());
        int taskIndex = InputUtils.inputNumber(in, "task index");
        if (!ArrayUtils.indexExists(tasks, taskIndex)){
            System.out.println("Task not found");
            return null;
        }

        return tasks.get(taskIndex).getId();
    }

}