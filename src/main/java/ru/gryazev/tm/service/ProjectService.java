package ru.gryazev.tm.service;

import ru.gryazev.tm.entity.Project;
import ru.gryazev.tm.repository.ProjectRepository;
import ru.gryazev.tm.repository.TaskRepository;
import ru.gryazev.tm.util.ArrayUtils;
import ru.gryazev.tm.util.InputUtils;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.List;

public class ProjectService{

    private ProjectRepository projectRepository = new ProjectRepository();

    private TaskRepository taskRepository = new TaskRepository();

    public void create(BufferedReader in) throws IOException {
        System.out.println("[PROJECT CREATE]");
        projectRepository.add(InputUtils.getProjectFromConsole(in));
        System.out.println("[OK]");
    }

    public void view(BufferedReader in) throws IOException {
        int index = InputUtils.inputNumber(in, "project index");
        List<Project> projects = projectRepository.getProjects();
        if (!ArrayUtils.indexExists(projects, index)){
            System.out.println("Project not found.");
            return;
        }
        System.out.println(projects.get(index));
    }

    public void list() {
        List<Project> projects = projectRepository.getProjects();
        for (int i = 0; i < projects.size(); i++)
            System.out.println(i + ". " + projects.get(i).getName());
    }

    public void edit(BufferedReader in) throws IOException {
        String projectId = getProjectId(in);
        if (projectId == null)
            return;

        Project editedProject = InputUtils.getProjectFromConsole(in);
        editedProject.setId(projectId);

        projectRepository.merge(editedProject);

        System.out.println("[OK]");
    }

    public void remove(BufferedReader in) throws IOException {
        String projectId = getProjectId(in);
        if (projectId == null)
            return;

        projectRepository.remove(projectId);
        taskRepository.removeTaskByProjectId(projectId);
        System.out.println("[DELETED]");
    }

    private String getProjectId(BufferedReader in){
        List<Project> projects = projectRepository.getProjects();

        int index = InputUtils.inputNumber(in, "project index");
        if (!ArrayUtils.indexExists(projects, index)) {
            System.out.println("Project not found");
            return null;
        }
        return projects.get(index).getId();
    }

    public ProjectRepository getProjectRepository() {
        return projectRepository;
    }

    public TaskRepository getTaskRepository() {
        return taskRepository;
    }

}
