package ru.gryazev.tm.util;

import ru.gryazev.tm.entity.Project;
import ru.gryazev.tm.entity.Task;

import java.io.BufferedReader;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;


public class InputUtils {

    public static int inputNumber(BufferedReader in, String source) {
        int id;
        System.out.println(String.format("Enter %s:", source));
        try {
            id = Integer.parseInt(in.readLine());
        } catch (NumberFormatException | IOException e) {
            System.out.println(String.format("Entered %s is incorrect.", source));
            return -1;
        }
        return id;
    }

    public static Date inputDate(BufferedReader in){
        SimpleDateFormat format = new SimpleDateFormat("d.MM.yyyy");
        Date result = null;
        try {
            result = format.parse(in.readLine());
        } catch (ParseException | IOException e){
            System.out.println("Entered date is incorrect. You can edit date later.");
        }
        return result;
    }

    public static Project getProjectFromConsole(BufferedReader in) throws IOException{
        Project project = new Project();
        System.out.println("Enter name:");
        project.setName(in.readLine());

        System.out.println("Enter details:");
        project.setDetails(in.readLine());

        System.out.println("Enter date of project start in DD.MM.YYYY format");
        Date startDate = InputUtils.inputDate(in);
        if (startDate != null)
            project.setDateStart(startDate);

        System.out.println("Enter date of project end in DD.MM.YYYY format");
        Date endDate = InputUtils.inputDate(in);
        if (endDate != null)
            project.setDateFinish(endDate);

        return project;
    }

    public static Task getTaskFromConsole(BufferedReader in) throws IOException{
        Task task = new Task();
        System.out.println("Enter name:");
        task.setName(in.readLine());

        System.out.println("Enter details:");
        task.setDetails(in.readLine());

        System.out.println("Enter date of task start in DD.MM.YYYY format");
        Date startDate = InputUtils.inputDate(in);
        if (startDate != null)
            task.setDateStart(startDate);

        System.out.println("Enter date of task end in DD.MM.YYYY format");
        Date endDate = InputUtils.inputDate(in);
        if (endDate != null)
            task.setDateFinish(endDate);

        return task;
    }

}
