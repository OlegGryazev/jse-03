package ru.gryazev.tm.util;

import java.text.SimpleDateFormat;
import java.util.Date;

public class DateUtils {

    private static SimpleDateFormat dateFormat = new SimpleDateFormat("d.MM.yyyy");

    public static String formatDateToString(Date date){
        return date != null ? dateFormat.format(date) : "undefined";
    }

}
