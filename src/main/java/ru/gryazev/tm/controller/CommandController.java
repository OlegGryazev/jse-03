package ru.gryazev.tm.controller;

import ru.gryazev.tm.enumerated.ConsoleCommand;
import ru.gryazev.tm.service.ProjectService;
import ru.gryazev.tm.service.TaskService;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;

public class CommandController {

    private ProjectService projectService = new ProjectService();

    private TaskService taskService = new TaskService(projectService.getProjectRepository(), projectService.getTaskRepository());

    private BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

    public void init(){
        try{
            String line;
            ConsoleCommand consoleCommand;
            while (true) {
                line = reader.readLine();
                try {
                    consoleCommand = ConsoleCommand.valueOf(line.replace('-', '_').toUpperCase());
                } catch(IllegalArgumentException e){
                    System.out.println("Command not found!");
                    continue;
                }
                if (consoleCommand == ConsoleCommand.EXIT)
                    return;
                executeCommand(consoleCommand);
            }
        } catch (IOException e){
            e.printStackTrace();
        } finally {
            try {
                reader.close();
            } catch (IOException e){
                e.printStackTrace();
            }
        }
    }

    public void executeCommand(ConsoleCommand consoleCommand) throws IOException {
        switch (consoleCommand) {
            case PROJECT_CREATE: projectService.create(reader);
                break;
            case PROJECT_LIST: projectService.list();
                break;
            case PROJECT_EDIT: projectService.edit(reader);
                break;
            case PROJECT_VIEW: projectService.view(reader);
                break;
            case PROJECT_REMOVE: projectService.remove(reader);
                break;
            case TASK_CREATE: taskService.create(reader);
                break;
            case TASK_VIEW: taskService.view(reader);
                break;
            case TASK_LIST: taskService.list(reader);
                break;
            case TASK_REMOVE: taskService.remove(reader);
                break;
            case TASK_EDIT: taskService.edit(reader);
                break;
            case HELP: showHelp();
                break;
            case EXIT: return;
            default:
                System.out.println("Command not found.");
        }
    }

    private void showHelp(){
        Arrays.stream(ConsoleCommand.values()).forEach(o -> System.out.println(o.getDescription()));
    }

}
