package ru.gryazev.tm;

import ru.gryazev.tm.controller.CommandController;

public class ProjectManager {

    public static void main(String[] args) {
        CommandController commandController = new CommandController();
        commandController.init();
    }

}
